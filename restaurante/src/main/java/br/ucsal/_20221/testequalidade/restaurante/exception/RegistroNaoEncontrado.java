package br.ucsal._20221.testequalidade.restaurante.exception;

public class RegistroNaoEncontrado extends Exception {

	private static final long serialVersionUID = 1L;

	public RegistroNaoEncontrado(String message) {
		super(message);
	}

}
